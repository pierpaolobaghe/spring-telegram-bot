package com.prisma.core.objects;

import lombok.Getter;
import lombok.Setter;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

@Getter
@Setter
public class CommandData {
    private String command;
    private List<String> args;
    private Update updateData;
    private UserData userData;
}
