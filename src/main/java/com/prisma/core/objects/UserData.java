package com.prisma.core.objects;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Comparator;

@Getter @Setter
public class UserData extends Item implements Serializable, Comparable<UserData> {
    private Long chatId;
    private Integer userId;
    private String userName;
    private String firstName;
    private String lastName;
    private boolean active;
    private boolean notifiable;

    @Override
    public int compareTo(UserData userData) {
        Comparator<String> nullsLast = Comparator
                .nullsLast(String::compareToIgnoreCase);
        return Comparator.comparing(UserData::getFirstName, nullsLast)
                .thenComparing(UserData::getLastName, nullsLast)
                .thenComparing(UserData::getUserName, nullsLast)
                .compare(this, userData);
    }
}