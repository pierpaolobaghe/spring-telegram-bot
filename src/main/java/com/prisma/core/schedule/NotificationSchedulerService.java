package com.prisma.core.schedule;

import com.prisma.core.services.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@EnableScheduling
@EnableAsync
@Component
public class NotificationSchedulerService {
    private static final Logger LOG = LoggerFactory.getLogger(NotificationSchedulerService.class);
    @Autowired
    private NotificationService notificationService;

    /**
     * Scheduled each 10 minutes to notify user.
     * Implement your custom logic
     */
    @Scheduled(cron = "*/10 * * * * *")
    public void notifyUsers() {
        //notificationService.notifyUser(user, "Crontab triggered!");
    }

}