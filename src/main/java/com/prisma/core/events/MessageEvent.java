package com.prisma.core.events;

import com.prisma.core.objects.UserData;
import org.apache.shiro.session.Session;
import org.springframework.context.ApplicationEvent;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Optional;

public class MessageEvent extends ApplicationEvent {
    private Update update;
    private Optional<Session> session;
    private UserData userData;

    public MessageEvent(Object source, Update message) {
        super(source);
        this.update = message;
    }

    public MessageEvent(Object source, Update message, Optional<Session> session) {
        super(source);
        this.update = message;
        this.session = session;
    }

    public MessageEvent(Object source, Update message, Optional<Session> session, UserData userData) {
        super(source);
        this.update = message;
        this.session = session;
        this.userData = userData;
    }

    public Update getUpdate() {
        return update;
    }

    public Optional<Session> getSession() {
        return session;
    }

    public UserData getUserData() {
        return userData;
    }
}
