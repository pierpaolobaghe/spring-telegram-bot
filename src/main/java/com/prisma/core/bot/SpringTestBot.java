package com.prisma.core.bot;

import com.prisma.core.data.BotConstants;
import com.prisma.core.events.MessageEvent;
import com.prisma.core.objects.UserData;
import com.prisma.core.services.UserService;
import org.apache.shiro.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.session.TelegramLongPollingSessionBot;

import java.util.Optional;

@Component
public class SpringTestBot extends TelegramLongPollingSessionBot {
    private static final Logger LOG = LoggerFactory.getLogger(SpringTestBot.class);

    @Autowired
    private ApplicationContext springContext;

    @Autowired
    private UserService userService;

    @Override
    public void onUpdateReceived(Update update, Optional<Session> session) {
        LOG.info("Publishing update event in ApplicationContext to MessageEvent listener!");
        UserData userData = userService.getUserData(update);
        MessageEvent messageEv = new MessageEvent(this, update, session, userData);
        springContext.publishEvent(messageEv);
    }

    @Override
    public String getBotUsername() {
        return BotConstants.PRISMA_BOT_NAME;
    }

    @Override
    public String getBotToken() {
        return BotConstants.PRISMA_BOT_CODE;
    }
}