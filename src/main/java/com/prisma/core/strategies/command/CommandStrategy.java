package com.prisma.core.strategies.command;

import com.prisma.core.objects.CommandData;

public interface CommandStrategy {
    void processCommand(CommandData commandData);
}
