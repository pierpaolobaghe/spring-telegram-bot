package com.prisma.core.strategies.command.impl;

import com.prisma.core.helpers.MessageComposer;
import com.prisma.core.objects.CommandData;
import com.prisma.core.strategies.command.CommandStrategy;
import com.prisma.core.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StartCommandStrategy implements CommandStrategy {
    @Autowired
    private MessageComposer messageComposer;

    @Override
    public void processCommand(CommandData commandData) {
        Util.sendMessage(messageComposer.getWelcomeMessage(commandData.getUserData()));
    }
}