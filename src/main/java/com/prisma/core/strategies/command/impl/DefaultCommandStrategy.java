package com.prisma.core.strategies.command.impl;

import com.prisma.core.data.BotConstants;
import com.prisma.core.helpers.MessageComposer;
import com.prisma.core.objects.CommandData;
import com.prisma.core.strategies.command.CommandStrategy;
import com.prisma.core.util.Util;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Component
public class DefaultCommandStrategy implements CommandStrategy {
    @Override
    public void processCommand(CommandData commandData) {
        String chatId = commandData.getUpdateData().getMessage().getChatId().toString();
        SendMessage message = MessageComposer.createMessage(chatId, BotConstants.WELCOME_MESSAGE);
        Util.sendMessage(message);
    }
}
