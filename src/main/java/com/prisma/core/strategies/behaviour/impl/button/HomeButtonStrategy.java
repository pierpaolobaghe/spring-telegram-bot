package com.prisma.core.strategies.behaviour.impl.button;

import com.prisma.core.events.MessageEvent;
import com.prisma.core.helpers.MessageComposer;
import com.prisma.core.strategies.behaviour.BehaviouralStrategy;
import com.prisma.core.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HomeButtonStrategy implements BehaviouralStrategy {
    private static final Logger LOG = LoggerFactory.getLogger(HomeButtonStrategy.class);

    @Autowired
    private MessageComposer messageComposer;

    @Override
    public void processUpdate(MessageEvent messageEvent) {
        Util.sendMessage(messageComposer.getWelcomeMessage(messageEvent.getUserData()));
        messageEvent.getSession().get().stop();
    }
}