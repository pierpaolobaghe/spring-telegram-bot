package com.prisma.core.strategies.behaviour.impl;

import com.prisma.core.data.BotButtonConstants;
import com.prisma.core.events.MessageEvent;
import com.prisma.core.strategies.behaviour.BehaviouralStrategy;
import com.prisma.core.strategies.behaviour.impl.button.HomeButtonStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class ChatStrategy implements BehaviouralStrategy {

    private final Map<String, BehaviouralStrategy> strategies = new HashMap<>();
    private static final Logger LOG = LoggerFactory.getLogger(ChatStrategy.class);

    @Autowired
    private ApplicationContext ctx;

    @PostConstruct
    public void init() {
        strategies.put(BotButtonConstants.HOME_BUTTON, ctx.getBean(HomeButtonStrategy.class));
    }

    @Override
    public void processUpdate(MessageEvent messageEvent) {
        Update update = messageEvent.getUpdate();
        //Bot default behavior
        LOG.info("Chat update received! Processing with Strategy Resolver.");
        BehaviouralStrategy strategy = this.getCommandSubmissionStrategy(messageEvent);

        strategy.processUpdate(messageEvent);
    }

    private BehaviouralStrategy getCommandSubmissionStrategy(MessageEvent messageEvent) {
        Update update = messageEvent.getUpdate();
        String key = update.getMessage().getText();
        BehaviouralStrategy strategyToUse = strategies.getOrDefault(key, ctx.getBean(FallbackStrategy.class));
        LOG.info("ChatStrategy found out to use the following behaviouralStrategy: " + strategyToUse.getClass().getSimpleName());
        return strategyToUse;
    }
}