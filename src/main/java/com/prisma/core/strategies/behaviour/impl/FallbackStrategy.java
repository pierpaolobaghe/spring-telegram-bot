package com.prisma.core.strategies.behaviour.impl;

import com.prisma.core.data.BotConstants;
import com.prisma.core.events.MessageEvent;
import com.prisma.core.helpers.MessageComposer;
import com.prisma.core.strategies.behaviour.BehaviouralStrategy;
import com.prisma.core.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class FallbackStrategy implements BehaviouralStrategy {

    private static final Logger LOG = LoggerFactory.getLogger(FallbackStrategy.class);

    @Override
    public void processUpdate(MessageEvent messageEvent) {
        Update update = messageEvent.getUpdate();
        //Bot default behavior
        Long chatId = messageEvent.getUserData().getChatId();
        LOG.info("Fallback strategy for unmapped command! " + update.getMessage().getText());
        SendMessage message = MessageComposer.createMessage(chatId);
        message.setText(BotConstants.COMMAND_NOT_FOUND);
        Util.sendMessage(message);
    }
}