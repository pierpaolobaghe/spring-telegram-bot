package com.prisma.core.strategies.behaviour.impl;

import com.prisma.core.events.MessageEvent;
import com.prisma.core.strategies.behaviour.BehaviouralStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class CallbackStrategy implements BehaviouralStrategy {

    private final Map<String, BehaviouralStrategy> strategies = new HashMap<>();
    private static final Logger LOG = LoggerFactory.getLogger(CallbackStrategy.class);

    @Autowired
    private ApplicationContext ctx;

    @PostConstruct
    public void init() {
        /* Add here with strategies.put -> Callback name + BehaviouralStrategy */
        //strategies.put("CallbackText", ctx.getBean(BehaviouralStrategy.class));
    }

    @Override
    public void processUpdate(MessageEvent messageEvent) {
        Update update = messageEvent.getUpdate();
        // Set variables
        BehaviouralStrategy strategy = this.getCallbackStrategy(messageEvent);
        strategy.processUpdate(messageEvent);
    }

    private BehaviouralStrategy getCallbackStrategy(MessageEvent messageEvent) {
        Update update = messageEvent.getUpdate();
        String key = update.getCallbackQuery().getData();
        BehaviouralStrategy strategyToUse = strategies.getOrDefault(key, ctx.getBean(FallbackStrategy.class));
        LOG.info("ChatStrategy found out to use the following callbackStrategy: " + strategyToUse.getClass().getSimpleName());
        return strategyToUse;
    }

}
