package com.prisma.core.strategies.behaviour;

import com.prisma.core.events.MessageEvent;

public interface BehaviouralStrategy {
    /*public void processUpdate(Update update);*/
    public void processUpdate(MessageEvent messageEvent);
}
