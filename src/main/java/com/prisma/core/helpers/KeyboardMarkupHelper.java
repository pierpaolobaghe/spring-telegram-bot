package com.prisma.core.helpers;

import com.prisma.core.data.BotButtonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.util.ArrayList;
import java.util.List;

public class KeyboardMarkupHelper {

    private static final Logger LOG = LoggerFactory.getLogger(KeyboardMarkupHelper.class);

    public static ReplyKeyboard getHomeKeyboard() {
        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        List<KeyboardRow> rows = new ArrayList<>();
        rows.add(getHomeRow());

        markup.setSelective(true);
        markup.setResizeKeyboard(true);
        markup.setKeyboard(rows);

        LOG.debug("Returning a KeyboardMarkup: " + markup);
        return markup;
    }

    private static KeyboardRow getHomeRow() {
        KeyboardRow homeRow = new KeyboardRow();
        homeRow.add(new KeyboardButton().setText(BotButtonConstants.HOME_BUTTON));
        return homeRow;
    }
}
