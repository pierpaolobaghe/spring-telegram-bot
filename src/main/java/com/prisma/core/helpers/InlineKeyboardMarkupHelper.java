package com.prisma.core.helpers;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

@Service
public class InlineKeyboardMarkupHelper {

    public static InlineKeyboardMarkup getLinkKeyboard(String text, String url){
        InlineKeyboardMarkup markup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rows = new ArrayList<>();
        List<InlineKeyboardButton> buttons = new ArrayList<>();

        InlineKeyboardButton linkButton = new InlineKeyboardButton().setText(text).setUrl(url);
        buttons.add(linkButton);

        rows.add(buttons);
        markup.setKeyboard(rows);
        return markup;
    }

}
