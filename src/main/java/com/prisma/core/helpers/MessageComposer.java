package com.prisma.core.helpers;

import com.prisma.core.data.BotConstants;
import com.prisma.core.objects.UserData;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Component
public class MessageComposer {

    public static SendMessage createMessage(String chatId, String message) {
        SendMessage sendMessage = new SendMessage().setChatId(chatId)
                .setText(message)
                .enableHtml(true);
        return sendMessage;
    }

    public static SendMessage createMessage(Long chatId) {
        SendMessage sendMessage = new SendMessage().setChatId(chatId)
                .enableHtml(true);
        return sendMessage;
    }

    public SendMessage getWelcomeMessage(UserData userData) {
        SendMessage sendMessage = new SendMessage()
                .setChatId(userData.getChatId())
                .setText(BotConstants.WELCOME_MESSAGE)
                .enableHtml(true)
                .setReplyMarkup(KeyboardMarkupHelper.getHomeKeyboard());
        return sendMessage;
    }
}
