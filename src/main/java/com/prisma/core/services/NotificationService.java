package com.prisma.core.services;

import com.prisma.core.data.BotConstants;
import com.prisma.core.helpers.InlineKeyboardMarkupHelper;
import com.prisma.core.helpers.MessageComposer;
import com.prisma.core.objects.UserData;
import com.prisma.core.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Service
public class NotificationService {
    private static final Logger LOG = LoggerFactory.getLogger(NotificationService.class);

    public void notifyUser(UserData data, String notification){
        LOG.info("Notifying user with id " + data.getUserId());
        SendMessage message = MessageComposer.createMessage(data.getChatId());
        message.setText(notification);
        if (data.isNotifiable()) {
            Util.sendMessage(message);
        } else {
            LOG.warn("User with id [" + data.getUserId() + "] is not notifiable! ");
        }
    }

    public void notifyUserForNullUsername(UserData data){
        LOG.info("Notifying user with id " + data.getUserId() + " of null username.");
        SendMessage message = MessageComposer.createMessage(data.getChatId());
        message.setText(BotConstants.NULL_USERNAME);
        message.setReplyMarkup(InlineKeyboardMarkupHelper.getLinkKeyboard("More info", BotConstants.TELEGRAM_USERNAME_URL));
        Util.sendMessage(message);
    }
}
