package com.prisma.core.services;

import com.prisma.core.objects.UserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;

@Service
public class UserService {
    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private NotificationService notificationService;

    public UserData getUserData(Update update) {
        UserData data = this.createUserData(update);

        if (this.hasNullUsername(data)) {
            notificationService.notifyUserForNullUsername(data);
        }

        return data;
    }

    public UserData createUserData(Update update) {
        Long chatId = this.resolveChatId(update);
        User user = this.resolveUpdateUser(update);
        UserData userData = new UserData();
        userData.setChatId(chatId);
        userData.setUserId(user.getId());
        userData.setFirstName(user.getFirstName());
        userData.setLastName(user.getLastName());
        userData.setUserName(user.getUserName());
        userData.setActive(true);
        return userData;
    }

    public boolean hasNullUsername(UserData userData){
        return userData.getUserName().isEmpty();
    }

    public User resolveUpdateUser(Update update) {
        User user = null;
        if (update.hasMessage()) user = update.getMessage().getFrom();
        else if (update.hasCallbackQuery()) user = update.getCallbackQuery().getFrom();
        return user;
    }

    public Long resolveChatId(Update update) {
        Long chatId = null;
        if (update.hasMessage()) chatId = update.getMessage().getChat().getId();
        else if (update.hasCallbackQuery()) chatId = update.getCallbackQuery().getMessage().getChat().getId();
        return chatId;
    }
}
