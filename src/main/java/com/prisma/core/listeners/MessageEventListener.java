package com.prisma.core.listeners;

import com.prisma.core.events.MessageEvent;
import com.prisma.core.routers.impl.CommandRouter;
import com.prisma.core.routers.impl.UpdateStrategyRouter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class MessageEventListener implements ApplicationListener<MessageEvent> {

    private static final Logger LOG = LoggerFactory.getLogger(MessageEventListener.class);

    @Autowired
    private ApplicationContext ctx;

    @Override
    public void onApplicationEvent(MessageEvent messageEvent) {
        LOG.info("EventListener received a MessageEvent update! Starting process.");

        if (messageEvent.getUpdate().hasMessage() && this.isCommand(messageEvent.getUpdate().getMessage().getText())){
            ctx.getBean(CommandRouter.class).routeUpdate(messageEvent);
        }else {
            ctx.getBean(UpdateStrategyRouter.class).routeUpdate(messageEvent);
        }
    }

    private boolean isCommand(String messageTxt) {
        return messageTxt.startsWith("/");
    }

}
