package com.prisma.core.data;

public class BotConstants {
    public static final String PRISMA_BOT_NAME = "";
    public static final String PRISMA_BOT_CODE = "";

    public static final String GEAR_EMOJI = "⚙️";
    public static final String INFO_EMOJI = "ℹ️ ";
    public static final String WARNING_EMOJI = "‼️";

    public static final String TELEGRAM_USERNAME_URL = "https://telegram.org/faq#q-what-are-usernames-how-do-i-get-one";

    public static final String COMMAND_NOT_FOUND = "Command not found/Mapped. Contact the support.";
    public static final String NULL_USERNAME = WARNING_EMOJI + " Your Telegram username is empty! " + WARNING_EMOJI + "\nPlease add an username to your profile, which allows the bot and to recognize you better.";

    public static final String WELCOME_MESSAGE = "<b>Welcome to the Spring test bot!</b>\n\uD83D\uDD3DGet started now!\uD83D\uDD3D";
    public static final String MOREINFO_MESSAGE = INFO_EMOJI + " <b>More information:</b> " + INFO_EMOJI + "\nAbout the bot:\nCreated by @theRealHerman using java telegrambots API + Spring.";
}
