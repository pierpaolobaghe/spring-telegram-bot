package com.prisma.core.util;

import com.prisma.core.bot.SpringTestBot;
import com.prisma.core.events.MessageEvent;
import com.prisma.core.objects.CommandData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageReplyMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class Util {
    private static Logger LOG = LoggerFactory.getLogger(Util.class);

    public static SpringTestBot telegramBot;

    public static CommandData getCommandData(MessageEvent messageEvent) {
        String commandTxt = messageEvent.getUpdate().getMessage().getText();
        CommandData commandData = new CommandData();
        commandData.setCommand(getCommandText(commandTxt));
        commandData.setArgs(getCommandArguments(commandTxt));
        commandData.setUpdateData(messageEvent.getUpdate());
        commandData.setUserData(messageEvent.getUserData());
        return commandData;
    }

    public static String getCommandText(String command) {
        String commandInstr = getCommandInstruction(command);
        return commandInstr.replaceAll("^\\/{1}", "");
    }

    public static String getCommandInstruction(String command) {
        return explodeCommandText(command).stream().findFirst().get();
    }

    public static List<String> getCommandArguments(String command) {
        return explodeCommandText(command).stream().skip(1).collect(Collectors.toList());
    }

    public static void resetInlineMarkup(MessageEvent event){
        Long chatId = event.getUserData().getChatId();
        Integer messageId = event.getUpdate().getCallbackQuery().getMessage().getMessageId();

        LOG.info("Resetting reply markup for message with id: " + messageId);

        EditMessageReplyMarkup editMessageReplyMarkup = new EditMessageReplyMarkup()
                .setChatId(chatId)
                .setMessageId(messageId)
                .setReplyMarkup(null);

        sendMessage(editMessageReplyMarkup);
    }

    /**
     * Index 0 contains the actual command, all the other indexes are arguments.
     *
     * @param command
     * @return
     */
    public static List<String> explodeCommandText(String command) {
        String[] splitArr = command.split("\\s+");
        List<String> splitCommand = Arrays.asList(splitArr);
        return splitCommand;
    }

    public static void sendMessage(BotApiMethod message) {
        try {
            telegramBot.execute(message);
        } catch (TelegramApiException exception) {
            LOG.error("Error while sending message!", exception);
        }
    }

    @Autowired
    public void setTelegramBot(SpringTestBot bot) {
        telegramBot = bot;
    }
}