package com.prisma.core.routers.impl;

import com.prisma.core.events.MessageEvent;
import com.prisma.core.routers.UpdateRouter;
import com.prisma.core.strategies.behaviour.BehaviouralStrategy;
import com.prisma.core.strategies.behaviour.impl.CallbackStrategy;
import com.prisma.core.strategies.behaviour.impl.ChatStrategy;
import com.prisma.core.strategies.behaviour.impl.FallbackStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;

@Component
public class UpdateStrategyRouter implements UpdateRouter {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateStrategyRouter.class);

    @Autowired
    private ApplicationContext ctx;

    @Override
    public void routeUpdate(MessageEvent messageEvent) {
        Update update = messageEvent.getUpdate();
        BehaviouralStrategy strategy = null;

        if (update.hasMessage()) {
            strategy = ctx.getBean(ChatStrategy.class);
        } else if (update.hasCallbackQuery()) {
            strategy = ctx.getBean(CallbackStrategy.class);
        } else {
            strategy = ctx.getBean(FallbackStrategy.class);
        }

        LOG.info("Handling Strategy is going to manage the event with the " + strategy.getClass().getSimpleName() + " pattern.");
        strategy.processUpdate(messageEvent);
    }
}
