package com.prisma.core.routers.impl;

import com.prisma.core.data.BotCommandConstants;
import com.prisma.core.events.MessageEvent;
import com.prisma.core.objects.CommandData;
import com.prisma.core.routers.UpdateRouter;
import com.prisma.core.strategies.behaviour.impl.ChatStrategy;
import com.prisma.core.strategies.command.CommandStrategy;
import com.prisma.core.strategies.command.impl.DefaultCommandStrategy;
import com.prisma.core.strategies.command.impl.StartCommandStrategy;
import com.prisma.core.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class CommandRouter implements UpdateRouter {

    private static final Logger LOG = LoggerFactory.getLogger(ChatStrategy.class);

    @Autowired
    private ApplicationContext applicationContext;

    private Map<String, CommandStrategy> commandStrategyMap = new HashMap<>();

    @PostConstruct
    private void populateCommandMap() {
        //Init available Strategies
        commandStrategyMap.put(BotCommandConstants.START_COMMAND, applicationContext.getBean(StartCommandStrategy.class));
    }

    @Override
    public void routeUpdate(MessageEvent messageEvent) {
        CommandData commandData = Util.getCommandData(messageEvent);
        LOG.info("Command data is being processed. /" + commandData.getCommand());
        CommandStrategy strategy = getStrategyForCommand(commandData);
        strategy.processCommand(commandData);
        LOG.info("Command was processed successfully using " + strategy.getClass().getSimpleName() + "!");
    }

    private CommandStrategy getStrategyForCommand(CommandData commandData) {
        String cmd = commandData.getCommand();
        boolean defaultCmd = commandStrategyMap.containsKey(cmd);
        CommandStrategy strategy = commandStrategyMap.getOrDefault(cmd, new DefaultCommandStrategy());

        LOG.info(!defaultCmd ? "Strategy not found! Falling back to default strategy." : "Strategy for handling command was found. ");
        return strategy;
    }

}
