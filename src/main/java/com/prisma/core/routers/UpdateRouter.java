package com.prisma.core.routers;

import com.prisma.core.events.MessageEvent;

public interface UpdateRouter {
    void routeUpdate(MessageEvent event);
}
