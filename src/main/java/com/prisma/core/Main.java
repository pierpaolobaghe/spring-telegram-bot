package com.prisma.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.ApiContextInitializer;

@SpringBootApplication
public class Main {

    private static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String... args) {
        /*Add config argument to arguments*/

        ApiContextInitializer.init();
        LOG.info("ApiContext of the bot was initialized.");
        SpringApplication.run(Main.class, args);
        LOG.info("Registered SpringApplication. Proceeding to register bot Methods");
    }
}