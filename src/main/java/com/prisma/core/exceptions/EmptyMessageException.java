package com.prisma.core.exceptions;

public class EmptyMessageException extends RuntimeException {
    public EmptyMessageException() {
        super();
    }

    public EmptyMessageException(String exc) {
        super(exc);
    }
}
