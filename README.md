# Documentation
Spring demonstration bot for TelegramBots users.

Created by [@theRealHerman](https://telegram.me/theRealHerman)
#### Architecture
Using Spring framework for EventHandling and AutoWire capacities.
TelegramBot --> onUpdateReceived
Publishes a new Event to SpringContext

listeners handles the event with a strategy pattern which chooses the best way to handle the message/the command

Message sending is handled via the Utils.sendMessage() method.
#### Launching
java -jar <name>.jar --spring.config.additional-location=file:./resources/

if you want to use a custom application.properties: first build application.properties file.

Feel free to contact me for any questions!